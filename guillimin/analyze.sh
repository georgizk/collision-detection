#!/bin/bash

CHECK_COLLISIONS=/home/georgi/collision-detection/bin/check_collisions_noshift
CHECK_COLLISIONS2=/home/georgi/collision-detection/bin/check_collisions_interleaved
BENCH_PATH=/gs/scratch/georgi/small_bench
BINLOG_PATH=/localscratch/$PBS_JOBID/small_bench/binlog
RESULTS_PATH=$BENCH_PATH/analysis
PROCESSED_PATH=${BINLOG_PATH}_processed
mkdir -p $RESULTS_PATH
mkdir -p $BINLOG_PATH
cd $BINLOG_PATH
arch=$1
filename=$(basename "$arch")
dir="${filename%.*}"

tar xf $arch
mkdir -p /gs/scratch/georgi/small_bench/analyzed_binlog
mv $arch $arch.processed 
#lock=analyzing.lock


#dir_count=`ls -1 | wc -l`
#if [[ $dir_count -eq 0 ]]; then
#    echo "no logs to analyze found"
#    exit
#fi
#nr=1
#dir=`ls -1 | head -n $nr | tail -n 1`


#FILES=./*
#shopt -s nullglob
#for f in $FILES
#do
	if [[ ! -d $dir ]]; then
exit #                continue
        fi
	files_count=`ls -1 $dir | wc -l`
	if [[ 0 == $files_count ]]; then
		rmdir $dir
exit #		continue
	fi
	nofaultarch=nofault.memonly.bin.tar.gz
	cd $dir
	nofault=`tar zxvf $nofaultarch`
	rm $nofaultarch
	if [[ -e $nofault ]]; then
		lock=$dir.analyzing.lock
		if [[ -e $lock ]]; then
    		  #pid=`cat $lock`
    		  #running=`ps -ef | grep $pid | grep analyze | wc -l`
    		  #if [[ $running -ne 0 || $pid = "" ]]; then
exit #		    continue
                  #else
		  #  rm $lock
		    #break
		  #fi
		#else
		  #break
 		fi
	fi
#done
if [[ ! -e $nofault ]]; then
    echo "couldn't locate nofault file"
    exit
fi

if [[ -e $lock ]]; then
    #pid=`cat $lock`
    #running=`ps -ef | grep $pid | grep analyze | wc -l`
    #if [[ $running -ne 0 || $pid = "" ]]; then
    #    echo "operation in progress. manually remove $BENCH_PATH/$lock"
        exit
    #fi
fi

echo $$ > $lock
#PROCESSED_PATH=$PROCESSED_PATH/$dir
#mkdir -p $PROCESSED_PATH
#mkdir -p $RESULTS_PATH

echo "Processed $files_count files in $BINLOG_PATH/$bin on `date +"%Y-%m-%d %HH%Mm%Ss"`" > $RESULTS_PATH/$dir.noshift.log
echo "Processed $files_count files in $BINLOG_PATH/$bin on `date +"%Y-%m-%d %HH%Mm%Ss"`" > $RESULTS_PATH/$dir.interleaved.log

FILES=./withfault*.tar.gz
shopt -s nullglob
for f in $FILES
do
     #echo $f
     bin=`tar zxvf $f`
     rm $f
     #echo $bin
     echo $bin >> $RESULTS_PATH/$dir.noshift.log
     $CHECK_COLLISIONS $nofault $bin >> $RESULTS_PATH/$dir.noshift.log

     echo $bin >> $RESULTS_PATH/$dir.interleaved.log
     $CHECK_COLLISIONS2 $nofault $bin >> $RESULTS_PATH/$dir.interleaved.log


#     if [[ -z $block_size ]]; then
#          block_size=`echo $output | awk '{ printf "%d",substr($1,2,10)}'`
#          boffset=$(( 3 + ${#block_size}  ))
#          echo $block_size
#     fi
#     blocks=`echo $output | awk '{ printf "%d",substr($1,"'${boffset}'",10)}'`
#     blocks_t=$(( $blocks + $blocks_t ))
#     echo $output
     rm $bin
done
#echo "total blocks $blocks_t"
rm $nofault
cd ..
#mv  $dir/* $PROCESSED_PATH
rm $dir/$lock

touch $dir/analyzed
rm -rf $dir
