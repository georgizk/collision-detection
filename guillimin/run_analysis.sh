USED_SPACE=`prquota | grep zcv-890-aa | awk '{printf "%.0f", $6}'`
MAX_USED=950 # the maximum percentage of the project space to use

if [[ $USED_SPACE -gt $MAX_USED ]]; then
  echo "Not enough free space - aborting"
  exit
else
  echo "Space is available - $USED_SPACE"
fi


RUNNING=`showq -u $USER | grep -i total | awk '{print $3}'`
MAX_JOBS=15;
if [[ $RUNNING -gt $MAX_JOBS ]]; then
  echo "$RUNNING job(s) running - more than $MAX_JOBS, aborting"
  exit
fi
BENCH_PATH=/sb/project/zcv-890-aa/small_bench
BINLOG_PATH=$BENCH_PATH/binlog_tar
RESULTS_PATH=$BENCH_PATH/analysis
PROCESSED_PATH=${BINLOG_PATH}_processed

cd $BINLOG_PATH
lock=analyzing.lock
dir_count=`ls -1 | wc -l`
if [[ $dir_count -eq 0 ]]; then
    echo "no logs to analyze found"
    exit
fi
nr=1
dir=`ls -1 | head -n $nr | tail -n 1`

FAULT_INSERTION_SCRATCH_PATH="/gs/scratch/$USER"
FAULT_INSERTION_BENCH_PATH="$HOME/collision-detection"

now=$(date +"%m%d%H%M%S")

mkdir -p ${FAULT_INSERTION_SCRATCH_PATH}/analysis/$now
i=1
time=$(($i + 50))
#while [[ $i -lt $time ]]; do
FILES=./*.tar
shopt -s nullglob
for f in $FILES
do
       # dir=$f
       # if [[ ! -d $dir ]]; then
       #         continue
       # fi
 	#files_count=`ls -1 $dir | wc -l`
        #if [[ 10001 != $files_count ]]; then
        #        continue
        #fi
        #nofaultarch=$dir/nofault.memonly.bin.tar.gz
        #nofault=`tar zxvf $nofaultarch`
        #if [[ -e $nofault ]]; then

echo "#!/bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l walltime=20:00:00
#PBS -o ${FAULT_INSERTION_SCRATCH_PATH}/analysis/$now/outputfile_$i
#PBS -e ${FAULT_INSERTION_SCRATCH_PATH}/analysis/$now/errorfile_$i
#PBS -V
#PBS -N $BENCH
#PBS -A zcv-890-aa

cd ${FAULT_INSERTION_BENCH_PATH}/guillimin

./analyze.sh ${BINLOG_PATH}/$f
" >${FAULT_INSERTION_SCRATCH_PATH}/analysis/$now/analysis_$i

msub -q sw ${FAULT_INSERTION_SCRATCH_PATH}/analysis/$now/analysis_$i

i=$(( i + 1 ))
if [[ $i -gt $time ]]; then
  exit
fi
#fi
done
#done
echo "Done starting jobs"
