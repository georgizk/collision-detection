benchmarks=( bitcount )
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BENCHMARKS_DIR="$HOME/collision-detection/guillimin"

USED_SPACE=`prquota | grep zcv-890-aa | awk '{printf "%.0f", $6}'`
MAX_USED=950 # the maximum percentage of the project space to use

if [[ $USED_SPACE -gt $MAX_USED ]]; then
  echo "Not enough free space - aborting"
  exit
else
  echo "Space is available - $USED_SPACE"
  RUNNING=`showq -u $USER | grep -i total | awk '{print $3}'`
  if [[ $RUNNING -ne 0 ]]; then
    echo "$RUNNING job(s) running - aborting"
    exit
  else
    # start iterating from given iteration number
    i=$1
    if [[ -e "${DIR}/runs" ]]; then
      i=`cat ${DIR}/runs`
    elif [[ -z "$1" ]]; then
      i=0
    fi
    MAX_RUNS=$(( ${#benchmarks[@]} * 10000 ))
    
    if [[ $i -ge $MAX_RUNS ]]; then
      echo "Has run $MAX_RUNS times already - aborting"
    else
      benchmark_index=$(( $i % ${#benchmarks[@]} ))
      benchmark=${benchmarks[$benchmark_index]}

      echo Running $benchmark

      cd $BENCHMARKS_DIR
      cd $benchmark

      ./start 

      echo $(( $i + 1 )) > ${DIR}/runs
      echo "Done starting jobs"
    fi # maximum runs check
  fi # running check
fi # space available check
