#!/bin/bash

CHECK_COLLISIONS=/data/gkosta/collision-detection/bin/check_collisions
BENCH_PATH=/data/gkosta/supercomp_data/small_bench
BINLOG_PATH=$BENCH_PATH/binlog
RESULTS_PATH=$BENCH_PATH/analysis
PROCESSED_PATH=${BINLOG_PATH}_processed

cd $BINLOG_PATH
lock=analyzing.lock
dir_count=`ls -1 | wc -l`
if [[ $dir_count -eq 0 ]]; then
    echo "no logs to analyze found"
    exit
fi
nr=1
dir=`ls -1 | head -n $nr | tail -n 1`


FILES=./*
shopt -s nullglob
for f in $FILES
do
	dir=$f
	if [[ ! -d $dir ]]; then
                continue
        fi
	files_count=`ls -1 $dir | wc -l`
	if [[ 0 == $files_count ]]; then
		rmdir $dir
		continue
	fi
	nofaultarch=$dir/nofault.memonly.bin.tar.gz
	nofault=`tar zxvf $nofaultarch`
	if [[ -e $nofault ]]; then
		break
	fi
done
if [[ ! -e $nofault ]]; then
    echo "couldn't locate nofault file"
    exit
fi

if [[ -e $lock ]]; then
    pid=`cat $lock`
    running=`ps -ef | grep $pid | grep analyze | wc -l`
    if [[ $running -ne 0 || $pid = "" ]]; then
        echo "operation in progress. manually remove $BENCH_PATH/$lock"
        exit
    fi
fi

echo $$ > $lock
PROCESSED_PATH=$PROCESSED_PATH/$dir
mkdir -p $PROCESSED_PATH
mkdir -p $RESULTS_PATH

echo "Processed $files_count files in $BINLOG_PATH/$bin on `date +"%Y-%m-%d %HH%Mm%Ss"`" >> $RESULTS_PATH/$dir.log

FILES=$dir/withfault*.tar.gz
shopt -s nullglob
for f in $FILES
do
     #echo $f
     bin=`tar zxvf $f`
     #echo $bin
     echo $bin >> $RESULTS_PATH/$dir.log
     $CHECK_COLLISIONS $nofault $bin >> $RESULTS_PATH/$dir.log
#     if [[ -z $block_size ]]; then
#          block_size=`echo $output | awk '{ printf "%d",substr($1,2,10)}'`
#          boffset=$(( 3 + ${#block_size}  ))
#          echo $block_size
#     fi
#     blocks=`echo $output | awk '{ printf "%d",substr($1,"'${boffset}'",10)}'`
#     blocks_t=$(( $blocks + $blocks_t ))
#     echo $output
     rm $bin
done
#echo "total blocks $blocks_t"
rm $nofault
mv  $dir/* $PROCESSED_PATH
rmdir $dir
rm $lock
