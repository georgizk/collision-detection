#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BENCHMARKS_STORE_PATH=/data/gkosta/supercomp_data
benchmarks=( small_bench )
index_b=0
while [ $index_b -lt ${#benchmarks[@]} ]; do
    USED_PERCENTAGE=`df -h | grep -i data | awk '{printf "%d",$5}'`
    MAX_PERCENTAGE=95
    rsync -e ssh -lzvrP georgi@guillimin.clumeq.ca:/sb/project/zcv-890-aa/${benchmarks[$index_b]}/dat $BENCHMARKS_STORE_PATH/${benchmarks[$index_b]} >> $DIR/rsync_output.log 2>&1 
    rsync -e ssh -lzvrP georgi@colosse.clumeq.ca:/scratch/zcv-890-aa/${benchmarks[$index_b]}/dat $BENCHMARKS_STORE_PATH/${benchmarks[$index_b]} >> $DIR/rsync_output.log 2>&1
    if [[ $USED_PERCENTAGE -lt $MAX_PERCENTAGE ]]; then
        rsync -e ssh -lzvrP --remove-source-files georgi@guillimin.clumeq.ca:/sb/project/zcv-890-aa/${benchmarks[$index_b]}/binlog $BENCHMARKS_STORE_PATH/${benchmarks[$index_b]} >> $DIR/rsync_output.log 2>&1
        rsync -e ssh -lzvrP --remove-source-files georgi@colosse.clumeq.ca:/scratch/zcv-890-aa/${benchmarks[$index_b]}/binlog $BENCHMARKS_STORE_PATH/${benchmarks[$index_b]} >> $DIR/rsync_output.log 2>&1
    else
        echo "not enough free space, not transferring" >> $DIR/rsync_output.log 2>&1
    fi 

    index_b=$(( $index_b + 1 ))
done
