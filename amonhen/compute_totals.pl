#!/usr/bin/perl -w

use POSIX;
use strict;
use warnings;
use File::Basename;
use Data::Dumper;

sub find_totals {
    my $path = shift;
    #print $path . "\n"; 
    my $line;
    my @vars;
    my @var_pairs;
    my $cumulative = shift;
    my $block;
    if (-e $path) {
	#print "opening\n";
        open(my $fh, '<:encoding(UTF-8)', $path) or die 'failed to open file';
    
        while (<$fh>) {
            chomp;
            $line = $_;
            @vars = split(' ',$line);
	    #print $vars[0] . "\n";
	    next unless $vars[0] =~ m/^b[0-9]+/;
	    @vars = reverse @vars;
            $block = pop @vars;
            @var_pairs = split(':',$block);
	    $block = $var_pairs[0];
            if (! defined ($cumulative->{$block})) {
		$cumulative->{$block} = {'count'=>0};
	    }
            $cumulative->{$block}->{'count'} += $var_pairs[1];
	    foreach (@vars) {


		@var_pairs = split(':',$_);
		#printf ("%s - %s\n",$var_pairs[0],$var_pairs[1]);
                $cumulative->{$block}->{$var_pairs[0]} += $var_pairs[1];
		
	    }

        }
        close $fh;
    }        
    return $cumulative;
}
my $analysis_dir_path = '/data/gkosta/supercomp_data/small_bench/analysis';
opendir(my $dh, $analysis_dir_path);
my $path;
my $totals = {};
while ($path = readdir($dh)) {
	next unless $path =~ m/\.log$/;
        $path = sprintf('%s/%s',$analysis_dir_path,$path);	
	print $path . "\n";
	$totals = find_totals($path,$totals);
	print Dumper($totals);
}


