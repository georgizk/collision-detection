#!/usr/bin/perl
use strict;
use warnings;
use File::Basename 'dirname';
use Cwd 'abs_path';

use lib abs_path(dirname(__FILE__)) . "/lib/perl5/site_perl/5.8.8";
use lib abs_path(dirname(__FILE__)) . "/lib64/perl5/site_perl/5.8.8/x86_64-linux-thread-multi";
use Expect;

my $exp = Expect->spawn('ssh georgi@colosse.clumeq.ca') or warn "unable to spawn: $!";

$exp->expect(60, '-re','\[georgi@.*\]\$') or die 'no connection';
# print "error";
# die;

$exp->send("~/collision-detection/colosse/run.sh\n");
unless ($exp->expect(5,'Space is available')) {
  $exp->send("logout\n");
  $exp->soft_close();
  exit;
}

unless ($exp->expect(5,'Running')) {
  $exp->send("logout\n");
  $exp->soft_close();
  exit;
}


$exp->expect(120,'Done starting jobs') or warn 'jobs not started';
$exp->send("logout\n");
$exp->soft_close();

